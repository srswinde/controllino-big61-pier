#include <Controllino.h>

#include <EEPROM.h>
#include <SPI.h>
#include <Ethernet.h>

#define RASERIAL Serial1
#define DECSERIAL Serial2

#define NO_ERROR 0
#define OBSID_ERROR 1
#define SYSID_ERROR 2
#define REFNUM_ERROR 3
#define QUERY_ERROR 4

#define OBSID "BIG61"
#define SYSID "PIER"

#define EEPROM_IPADDR  0

#define RESPTIMEOUT 2000


//NG communication stuff
const char * ecode[] = {
    "NO ERROR",
    "OBSID ERROR",
    "SYSID ERROR",
    "REFNUM ERROR",
    "QUERY ERROR"
};

struct serial_data
{
  char queryType[10];
  char cmd[20];
  char args[4][50];
  unsigned short arg_count;
};
struct serial_data sdata;
char serial_in[100];
unsigned short serial_in_counter=0;

struct ng_data
{
  char sysid[20];
  char obsid[10];
  unsigned short refNum;
  char queryType[10];
  unsigned short arg_count;  
  char args[10][50];
  
};

struct ng_data data;
struct ng_data currCom;
char ng_response[200];
//End NG Communication stuff.

byte ip[4];
//THis Should probably storedin the EEPROM
byte gateway[] = { 128,196,208,1};
byte subnet[] = { 255, 255, 0, 0 };

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };


//Drive Communcication states
enum driveComState
{
  SENDING,
  READING,
  
};
enum driveComState raDriveCom = SENDING;
enum driveComState decDriveCom = SENDING;

//Serial Ports are read one byte at
// at a time. 
int raByte=0;
int decByte=0;

char raResp[20];
char decResp[20];
unsigned short raRespIndex = 0;
unsigned short decRespIndex = 0;


//The Copley RS-232 command to 
// Retrieve drive currents. 
char get_current_cmd[] = "g r0x30\r";


int counter = 0;



char ngrawin[200];

char charcode;
int raCurrent;
int decCurrent;
char dummy[100];
unsigned long raRespTimer;
unsigned long decRespTimer;
EthernetServer server = EthernetServer(5750);






void setup() 
{
  //Serial is the USB interface
  // We use it for debugging and 
  // setting the IP address
  Serial.begin(9600);
  //Serial1.begin(115200, SERIAL_8N1);

  /*Serial2 is the kopley accelnet drive
  On start up the drive runs at 9600 baude
  but can be made faster by setting
  the 0x90 register. For now we will 
  leave it at 9600 for simplicity.*/
  RASERIAL.begin(9600, SERIAL_8N1);
  DECSERIAL.begin(9600, SERIAL_8N1);

  //Here is what we would do if we
  //wanted to increate the baud rate
  //Serial2.write("s r0x90 115200\r");
  //delay(100);
  //Serial2.begin(115200, SERIAL_8N1);
  //delay(100);
 

  ip[0] = EEPROM.read(EEPROM_IPADDR);
  ip[1] = EEPROM.read(EEPROM_IPADDR+1);
  ip[2] = EEPROM.read(EEPROM_IPADDR+2);
  ip[3] = EEPROM.read(EEPROM_IPADDR+3);

  Ethernet.begin(mac, ip, gateway, subnet );
  

}
void loop()
{

      int charCounter=0;
        
      switch(raDriveCom)
      {
        case SENDING:
          RASERIAL.write(get_current_cmd);
          raDriveCom = READING;
          
          
          raRespTimer = millis();
         
        break;

        case READING:
          
          if (RASERIAL.available() > 0)
          {
            
            raByte = RASERIAL.read();
            
            if(raByte == '\r')
            {
             
              raDriveCom = SENDING;
              

              raRespIndex=0;
              if(raResp[0] == 'v')
              {
                sscanf(raResp, "%c %d", &charcode, &raCurrent);
              }

            }
            else
            {
              raResp[raRespIndex] = raByte;
              raRespIndex++;
            }
          }
          else
          {
            if( (millis() - raRespTimer) > RESPTIMEOUT )
            {
              //RA Drive didn't respond try again.
              raDriveCom = SENDING;
              
            }
            
            
          }
      }

      //Dec Current
      switch(decDriveCom)
      {
        case SENDING:
          DECSERIAL.write(get_current_cmd);
          decDriveCom = READING;
          
          
          decRespTimer = millis();
         
        break;

        case READING:
          
          if (DECSERIAL.available() > 0)
          {
            
            decByte = DECSERIAL.read();
            
            if(decByte == '\r')
            {
             
              decDriveCom = SENDING;
              
              
              decRespIndex=0;
              if(decResp[0] == 'v')
              {//IF no error
               
                sscanf(decResp, "%c %d", &charcode, &decCurrent);
              }

            }
            else
            {
              decResp[decRespIndex] = decByte;
              decRespIndex++;
            }
          }
          else
          {
            if( (millis() - decRespTimer) > RESPTIMEOUT )
            {
              //Dec Drive didn't respond try again.
              decDriveCom = SENDING;

              
            }
            
            
          }
      }
      


  EthernetClient client = server.available();

  while (client.connected()) 
  {
    
   // if an incoming client connects, there will be bytes available to read:
    if (client.available())
    {
      // read bytes from the incoming client and write them back
      // to any clients connected to the server:
      char c = client.read();
      if (c == '\n')
      {
        
        int syntaxLIMITSTATE_ERROR = parseNG(ngrawin, &currCom);
        
        if ( syntaxLIMITSTATE_ERROR == NO_ERROR )
        {
          handle_input(&currCom).toCharArray(dummy, 100);
          sprintf(ng_response, "%s %s %i %s", OBSID, SYSID, currCom.refNum, dummy );
        }
        else
          sprintf(ng_response, "%s %s %i %s", ecode[syntaxLIMITSTATE_ERROR] );
          
        client.println(ng_response);
        ng_response[0]='\0';
        ngrawin[0] = '\0';
        client.stop();

      }
      else
      {
        ngrawin[ charCounter ] = c;
        ngrawin[charCounter+1] = '\0';
        charCounter++;

      }
    }
  }
  
  while(Serial.available()>0)
  {
    
    char sc=Serial.read();
    if (sc == '\n')
    {
      serial_in[serial_in_counter] = '\0';
      sprintf(ngrawin, "%s %s 123 %s\0", OBSID, SYSID, serial_in );
      Serial.println(ngrawin);
      int syntaxLIMITSTATE_ERROR = parseNG(ngrawin, &currCom);
      
      printNG(&currCom);
      
        
        if ( syntaxLIMITSTATE_ERROR == NO_ERROR )
        {
          handle_input(&currCom).toCharArray(dummy, 100);
          sprintf(ng_response, "%s %s %i %s", OBSID, SYSID, currCom.refNum, dummy );
        }
          
        else
          sprintf(ng_response, "%s %s %s", OBSID, SYSID, ecode[syntaxLIMITSTATE_ERROR] );
          
        Serial.println(ng_response );
          
      
      ngrawin[0] = '\0';
      serial_in[0] = '\0';
      serial_in_counter = 0;
    }
    else
    {
      serial_in[serial_in_counter] = sc;
      serial_in_counter++;
    }
    
  }
  
  
}



int parseNG( char inRaw[] , struct ng_data *parsed )
{
  short word_count = 0;
  char *tok;

  int errorState = 0;

  int refNum;
  char queryType[10];
  char obsid[10];
  char sysid[10];

  tok = strtok(inRaw, " \t");
  
  while( tok != NULL )
  {  
    
    switch(word_count)
    {
      case 0://observation ID
        strcpy( obsid, tok );
        if( strcmp( obsid, OBSID ) == 0)
          strcpy( parsed->obsid, tok );
        else
            errorState = 1;
        break;
        
      case 1://system ID
        strcpy( sysid, tok );
        
        if( strcmp( sysid, SYSID ) == 0 )
          strcpy( parsed->sysid, tok );
        
        else
          errorState = 2;
        break;
      
      case 2://reference number
        refNum = atoi( tok );
          if( refNum > 0 )
            parsed->refNum = refNum;
          else 
          errorState = 3;
          break;
        
      case 3://query type
        strcpy( queryType, tok );
        
        if ( ( strcmp(queryType, "COMMAND") == 0) || ( strcmp(queryType, "REQUEST") == 0) )
          strcpy( parsed->queryType, queryType );
        else
          errorState = 4;
        break;

      default://Arguments
        strcpy( parsed->args[word_count - 4], tok  );
        
    }
    tok = strtok( NULL, " \t" );
    word_count++;
  }
  parsed->arg_count = word_count - 1;
  return errorState;
}

String handle_input( struct ng_data *ngInput )
{
  //char helper[50] ;
  String resp = "UNKOWN COMMAND ";
  char ipaddr[18];
  byte octets[4];
  int sscanf_resp;
  resp = resp+ngInput->queryType+" "+ngInput->args[0];
  if ( strcmp( ngInput->queryType, "COMMAND" ) == 0 )
  {
      if( strcmp( ngInput->args[0], "IPADDR" ) == 0 )
      {     
        if(sscanf(ngInput->args[1], "%i.%i.%i.%i", &octets[0], &octets[1], &octets[2], &octets[3]) != 4)
        {
          resp ="BAD IP ADDRESS";
        }
        else
        {
          EEPROM.write(EEPROM_IPADDR, octets[0]);
          EEPROM.write(EEPROM_IPADDR+1, octets[1]);
          EEPROM.write(EEPROM_IPADDR+2, octets[2]);
          EEPROM.write(EEPROM_IPADDR+3, octets[3]);
        }
        resp="OK";
      }
    }
    
  
  else if( strcmp(  ngInput->queryType, "REQUEST" ) == 0 )
  {
    
    if( strcmp(  ngInput->args[0], "IPADDR" ) == 0 )
    {
      sprintf(ipaddr, "%i.%i.%i.%i",EEPROM.read(EEPROM_IPADDR), EEPROM.read(EEPROM_IPADDR +1),EEPROM.read(EEPROM_IPADDR +2),EEPROM.read(EEPROM_IPADDR +3) ); 
      resp=ipaddr;
    }
    if( strcmp(  ngInput->args[0], "CURRENT" ) == 0 )
    {
      if( strcmp(ngInput->args[1], "RA") == 0 )
        resp=raCurrent;
      else if( strcmp(ngInput->args[1], "DEC") == 0 )
      {
        resp=decCurrent;
      }
    }
  }
  else
  {
    resp="BAD";
  } 
  
  return resp;
}

void printNG(struct ng_data *dt)
{
  Serial.print( "OBSID=> " );
  Serial.println( dt->obsid );
  Serial.print( "SYSID=> " );
  Serial.println(dt->sysid);
  Serial.print( "qt=> " );
  Serial.println( dt->queryType );
  Serial.print("arg_count=> ");
  Serial.println(dt->arg_count);
  for( int ii=0; ii<dt->arg_count-3; ii++ )
  {
    Serial.print( "arg=> " );
    Serial.println( dt->args[ii] );
  }
  
}
