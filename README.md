### Pier Controllino Firmware

This is the firmware for the controllino slated to be put on the Kuiper 61" pier. It will give network access to anything we deem necessary on the pier of the 61". Currenlty the firmware is designed to poll the kopley drive units and make the available throught the NG network interface. 

## Accessing Drive currents

To get the drive currents over the network use the [ng protocal](https://lavinia.as.arizona.edu/~tscopewiki/doku.php?id=ng_protocol) to request CURRENT like:

BIG61 PIER 234 REQUEST CURRENT

Send this over the NG port using TCP and be sure to end it with a newline. It will respond witht the current in milli Amps. 

